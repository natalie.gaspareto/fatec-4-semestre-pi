from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^alterar-dados/$', views.UpdateUserView.as_view(template_name='update_user.html'), name='update_user'),
    url(r'^alterar-senha/$', views.UpdatePasswordView.as_view(template_name='update_password.html'), name='update_password'),
    url(r'^registro/$', views.RegisterView.as_view(template_name='register.html'), name='register'),
]

app_name = "accounts"